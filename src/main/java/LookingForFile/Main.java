package LookingForFile;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        searchFile("c:", "pom.xml");
    }

    private static void searchFile(String path, String name) {
        File file = new File(path);
        if (file.isDirectory() && file.exists()) {
            File[] listofFiles = file.listFiles();
            for (File f : listofFiles) {
                if (f.getName().startsWith(name)) {
                    System.out.println(f.getAbsolutePath());
                }
                if (f.isDirectory()) {
                    searchFile(f.getAbsolutePath(), name);
                }
            }
        }
    }
}
