package Zadanie_4;

public class Zigzag implements IMovementStyle {
    private boolean direction = true;
    private double currentY = 0.0;
    private int counter = 0;

    @Override
    public Point getNewPointX() {
        if (direction) {
            counter++;
            if (counter == 50) {
                counter = 0;
                direction = false;
            }
            return new Point(1.0, -1.0);
        } else {
            counter++;
            if (counter == 50) {
                counter = 0;
                direction = true;
            }
            return new Point(1.0, 1.0);
        }
    }
}