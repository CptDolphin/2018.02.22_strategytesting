package Zadanie_4;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Worm implements Runnable {
    private double x = 0.0;
    private double y = 0.0;
    private Worm worm;
    private IMovementStyle style = new LinesStyleMovemenet();
    private ExecutorService watek = Executors.newSingleThreadExecutor();

    public Worm(){
        watek.submit(this);
    }

    public void generateMove(){
        Point whereTo = style.getNewPointX();
        this.x += whereTo.getX();
        this.y += whereTo.getY();
        saveToFile();
    }

    public void setStyle(IMovementStyle style) {
        this.style = style;
    }

    private void saveToFile(){
        try(PrintWriter printWriter = new PrintWriter(new FileWriter("data.txt"),true)) {
            printWriter.println(this.x + ";" + this.y);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean isWorking = true;
        while (isWorking) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            worm.generateMove();
            worm.generateMove();
            worm.generateMove();
        }
    }
}
