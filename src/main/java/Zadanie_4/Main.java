package Zadanie_4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Worm worm = new Worm();
        Scanner scanner = new Scanner(System.in);
        boolean isWorking = true;
        while(isWorking){
            String line = scanner.nextLine();
            if(line.equals("1")){
                worm.setStyle(new LinesStyleMovemenet());
            }else if (line.equals("2")){
                worm.setStyle(new Zigzag());
            }
        }
    }
}