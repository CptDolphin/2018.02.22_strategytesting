package Zadanie_3;

import Zadanie_3.TrybyJazdy.Eco;
import Zadanie_3.TrybyJazdy.Normal;
import Zadanie_3.TrybyJazdy.Turbo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Samochod samochod = new Samochod(100,15,20,10);

        boolean isWorking = true;
        while(isWorking){
            System.out.println("Jakim stylem chcesz jechac? normal/eco/turbo");
            String tryb = scanner.nextLine();
            if(tryb.equals("normal")){
                samochod.setStrategy(new Normal());
            }else if(tryb.equals("eco")){
                samochod.setStrategy(new Eco());
            }else if(tryb.equals("turbo")){
                samochod.setStrategy(new Turbo());
            }
            System.out.println("Czy chcesz przyszpieszyc? ");
            String command = scanner.nextLine();
            if(command.equals("tak")){
                samochod.increaseSpeed();
            }if(command.equals("nie")){
                samochod.decreasedSpeed();
            }
            if(command.equals("quit") && (tryb.equals("quit"))){
                System.exit(0);
            }
            samochod.writeCurrentUsageOfAllParameters();
        }
    }
}
