package Zadanie_3.TrybyJazdy;

import Zadanie_3.ISamochodStrategy;

public class Normal implements ISamochodStrategy {
    public double increasedSpeed() {
        return 10;
    }

    public double decreasedSpeed() {
        return 10;
    }

    public double getGasReleased() {
        return 0.2;
    }

    public double getEngineWear() {
        return 0.1;
    }

    public double getBreaksWear() {
        return 0.5;
    }

    @Override
    public String toString() {
        return "Normal";
    }
}
