package Zadanie_3.TrybyJazdy;

import Zadanie_3.ISamochodStrategy;

public class Eco implements ISamochodStrategy {
    public double increasedSpeed() {
        return 3;
    }

    public double decreasedSpeed() {
        return 3;
    }

    public double getGasReleased() {
        return 0.01;
    }

    public double getEngineWear() {
        return 0.1;
    }

    public double getBreaksWear() {
        return 0.01;
    }

    @Override
    public String toString() {
        return "Eco";
    }
}
