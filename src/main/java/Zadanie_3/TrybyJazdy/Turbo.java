package Zadanie_3.TrybyJazdy;

import Zadanie_3.ISamochodStrategy;

public class Turbo implements ISamochodStrategy {
    public double increasedSpeed() {
        return 30;
    }

    public double decreasedSpeed() {
        return 30;
    }

    public double getGasReleased() {
        return 1;
    }

    public double getEngineWear() {
        return 1;
    }

    public double getBreaksWear() {
        return 3;
    }

    @Override
    public String toString() {
        return "Turbo";
    }
}
