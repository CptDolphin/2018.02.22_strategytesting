package Zadanie_3;

public class Samochod {
    private int speed;
    private int engineWear;
    private int breaksWear;
    private int gasReleased;
    private ISamochodStrategy strategy;

    public Samochod(int speed, int engineWear, int breaksWear, int gasReleased) {
        this.speed = speed;
        this.engineWear = engineWear;
        this.breaksWear = breaksWear;
        this.gasReleased = gasReleased;
    }

    public int getEngineWear() {
        return engineWear;
    }

    public int getBreaksWear() {
        return breaksWear;
    }

    public int getGasReleased() {
        return gasReleased;
    }


    public void writeCurrentUsageOfAllParameters() {
        System.out.println("Tryb: " + strategy.toString());
        System.out.println("Speed: " + speed);
        System.out.println("Engine wear: " + engineWear);
        System.out.println("Breaks Wear: " + breaksWear);
        System.out.println("Gas Released: " + gasReleased);
    }

    public void increaseSpeed() {
        speed += strategy.increasedSpeed(); // predkosc
        engineWear += strategy.getEngineWear();
        breaksWear += strategy.getBreaksWear();
        gasReleased += strategy.getGasReleased();
    }

    public void decreasedSpeed() {
        speed -= strategy.increasedSpeed(); // predkosc
        engineWear -= strategy.getEngineWear();
        breaksWear -= strategy.getBreaksWear();
        gasReleased -= strategy.getGasReleased();
    }

    public void setStrategy(ISamochodStrategy strategy){
        this.strategy = strategy;
    }

}

