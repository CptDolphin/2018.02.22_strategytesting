package Zadanie_3;

public interface ISamochodStrategy {
    double increasedSpeed();

    double decreasedSpeed();

    double getGasReleased();

    double getEngineWear();

    double getBreaksWear();
}
