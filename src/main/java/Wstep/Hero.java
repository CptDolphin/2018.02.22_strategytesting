package Wstep;

import Wstep.Stratregies.StrategyKnight;

public class Hero {
    private String name;
    private int health;
    private int power;

    private IStrategy stylWalki;

    public Hero(String name, int health, int power) {
        this.name = name;
        this.health = health;
        this.power = power;
        this.stylWalki  = new StrategyKnight();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public IStrategy getStylWalki() {
        return stylWalki;
    }

    public void setStylWalki(IStrategy stylWalki) {
        this.stylWalki = stylWalki;
    }

    public void fight(){
        stylWalki.fight();
    }
}