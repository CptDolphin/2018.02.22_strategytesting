package Wstep;

import Wstep.Stratregies.StrategyArcher;

public class Main {
    public static void main(String[] args) {
        Hero h = new Hero("Benek",100,200);

        h.fight();
        h.setStylWalki(new StrategyArcher());
        h.fight();
    }
}
