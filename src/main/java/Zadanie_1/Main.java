package Zadanie_1;

import Zadanie_1.Settings.HDSetting;

public class Main {
    public static void main(String[] args) {
        GraphicsCard graphicsCard = new GraphicsCard();
        int[][] ramka = new int[10][10];

        wypiszRamke(ramka);

        graphicsCard.setSettings(new HDSetting());
        graphicsCard.processFrame(ramka);
        wypiszRamke(ramka);
    }


    public static void wypiszRamke(int[][] frame){
        for (int i = 0; i < frame.length; i++) {
            for (int j = 0; j < frame[i].length; j++) {
                System.out.print(frame[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
