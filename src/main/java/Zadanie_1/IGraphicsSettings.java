package Zadanie_1;

public interface IGraphicsSettings {
    int getNeededProcessingPower();
    void processFrame(int[][] frame);
}
