package Zadanie_1;


public class GraphicsCard {
    private IGraphicsSettings settings;

    public void setSettings(IGraphicsSettings settings) {
        this.settings = settings;
    }

    public GraphicsCard() {
        this.settings = settings;
    }

    public void processFrame(int[][] frame){
        settings.processFrame(frame);
    }
}
