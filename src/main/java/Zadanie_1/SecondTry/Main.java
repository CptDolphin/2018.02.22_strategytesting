package Zadanie_1.SecondTry;

import Zadanie_1.Settings.HDSetting;
import Zadanie_1.Settings.LowSetting;
import Zadanie_1.Settings.MediumSetting;

public class Main {
    public static void main(String[] args) {
        GraphicsCard card= new GraphicsCard();
        int[][] frame = new int[10][10];

        card.setiGraphicsSettings(new HDSetting());
        card.processFrame(frame);

        for (int i = 0; i < frame.length; i++) {
            for (int j = 0; j < frame.length; j++) {
                System.out.print(frame[i][j] + " ");
            }
            System.out.println();
        }
    }
}
