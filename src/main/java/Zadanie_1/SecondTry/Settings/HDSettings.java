package Zadanie_1.SecondTry.Settings;

import Zadanie_1.SecondTry.IGraphicsCard;

public class HDSettings implements IGraphicsCard {
    @Override
    public void processFrame(int[][] frame) {
        for (int i = 0; i < frame.length; i++) {
            for (int j = 0; j < frame[i].length; j++) {
                frame[i][j] = 3;
            }
        }
    }
}
