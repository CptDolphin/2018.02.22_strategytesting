package Zadanie_1.SecondTry;

import Zadanie_1.IGraphicsSettings;

public class GraphicsCard {
    private IGraphicsSettings iGraphicsSettings;

    public void setiGraphicsSettings(IGraphicsSettings iGraphicsSettings) {
        this.iGraphicsSettings = iGraphicsSettings;
    }

    public GraphicsCard() {
        this.iGraphicsSettings = iGraphicsSettings;
    }

    public void processFrame(int[][] frame){
        iGraphicsSettings.processFrame(frame);
    }
}
