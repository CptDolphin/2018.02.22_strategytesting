package Zadanie_1.Settings;

import Zadanie_1.IGraphicsSettings;
import Zadanie_1.Settings.notUsed.Obraz;

public class MediumSetting extends Obraz implements IGraphicsSettings {
    public int getNeededProcessingPower() {
        return 52;
    }

    public void processFrame(int[][] frame) {
        for (int i = 0; i < frame.length; i++) {
            for (int j = 0; j < frame[i].length; j++) {
                frame[i][j] = 2;
            }
        }
    }
}
