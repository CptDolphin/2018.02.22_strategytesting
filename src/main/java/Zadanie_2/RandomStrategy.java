package Zadanie_2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.UUID;

public class RandomStrategy implements InputReader{
    public static final Random random = new Random();
    @Override
    public int getInt() {
        try(PrintWriter printWriter = new PrintWriter(new FileWriter("pom_tmp.txt",true))) {
            int tmp = random.nextInt(50);
            printWriter.println(tmp);
            printWriter.flush();
            return tmp;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public String getString() {
        try(PrintWriter printWriter = new PrintWriter(new FileWriter("pom_tmp.txt",true))) {
            String tmp = String.valueOf(UUID.randomUUID());
            printWriter.println(tmp);
            printWriter.flush();
            return tmp;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public double getDouble() {
        try(PrintWriter printWriter = new PrintWriter(new FileWriter("pom_tmp.txt",true))) {
            double tmp = random.nextDouble();
            printWriter.println(String.valueOf(tmp));
            printWriter.flush();
            return tmp;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
