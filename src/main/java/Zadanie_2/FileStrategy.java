package Zadanie_2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileStrategy implements InputReader{

    @Override
    public int getInt() {
        try (BufferedReader fileReader = new BufferedReader(new FileReader("pom_tmp.txt"))){
            String line = fileReader.readLine();
            return Integer.parseInt(line);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch( IllegalArgumentException ie){
            ie.printStackTrace();
        }
        return 0;
    }

    @Override
    public String getString() {
        try (BufferedReader fileReader = new BufferedReader(new FileReader("pom_tmp.txt"))){
            String line = fileReader.readLine();
            return line;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public double getDouble() {
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader("pom_tmp.txt"));
            String line = fileReader.readLine();
            return Double.parseDouble(line);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
