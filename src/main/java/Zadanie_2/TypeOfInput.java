package Zadanie_2;

public class TypeOfInput implements InputReader {
    private InputReader inputReader;


    public void setInputReader(InputReader inputReader) {
        this.inputReader = inputReader;
    }

    public InputReader getInputReader() {
        return inputReader;
    }

    public TypeOfInput() {
    }

    @Override
    public int getInt() {
        return inputReader.getInt();
    }

    @Override
    public String getString() {
        return inputReader.getString();
    }

    @Override
    public double getDouble() {
        return inputReader.getDouble();
    }
}
