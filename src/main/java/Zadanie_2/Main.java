package Zadanie_2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isWorking = true;
        int counter = 0;

        System.out.println("W jaki sposob chcesz zaladowac aplikacje ?");
        String sposob = scanner.nextLine();
        TypeOfInput typeOfInput = new TypeOfInput();
        while (isWorking) {

            if (sposob.toLowerCase().trim().equals("file")) {
                typeOfInput.setInputReader(new FileStrategy());
            } else if (sposob.toLowerCase().trim().equals("random")) {
                typeOfInput.setInputReader(new RandomStrategy());
            } else if (sposob.toLowerCase().trim().equals("std")) {
                typeOfInput.setInputReader(new Std_inStrategy());
            }

            System.out.println("Co chcesz zrobic: ");
            String coRobic = scanner.nextLine();
            if (coRobic.trim().toLowerCase().equals("getint")) {
                System.out.println(typeOfInput.getInt());
            } else if (coRobic.trim().toLowerCase().equals("getstring")) {
                System.out.println(typeOfInput.getString());
            } else if (coRobic.trim().toLowerCase().equals("getdouble")) {
                System.out.println(typeOfInput.getDouble());
            } else if (coRobic.equals("quit")) {
                System.exit(0);
            }
        }
    }
}