package Zadanie_2;

import java.io.*;
import java.util.Scanner;

public class Std_inStrategy implements InputReader {
    public static final Scanner scanner = new Scanner(System.in);

    @Override
    public int getInt() {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter("pom_tmp.txt",true))) {
            int line = scanner.nextInt();
            printWriter.println(line);
            printWriter.flush();
            return line;
        } catch (NullPointerException ne) {
            System.err.println("NullPointer found");
            return 0;
        } catch (IllegalArgumentException ie) {
            System.err.println("Illegal argument");
            return 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public String getString() {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter("pom_tmp.txt",true))){
            String line = scanner.nextLine();
            printWriter.println(line);
            printWriter.flush();
            return line;
        } catch (NullPointerException ne) {
            System.err.println("NullPointer found");
        } catch (IllegalArgumentException ie) {
            System.err.println("Illegal argument");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public double getDouble() {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter("pom_tmp.txt",true))) {
            Double line = scanner.nextDouble();
            printWriter.println(String.valueOf(line));
            printWriter.flush();
            return line;
        } catch (NullPointerException ne) {
            System.err.println("NullPointer found");
            return 0;
        } catch (IllegalArgumentException ie) {
            System.err.println("Illegal argument");
            return 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
