package Zadanie_2;

public interface InputReader {
    int getInt();
    String getString();
    double getDouble();
}
